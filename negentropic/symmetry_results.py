from negentropic.negentropic_test import sample_sizes
from negentropic.competitors import *

case_4 = [
    [0.258, 0.280, 0.276],
    [0.394, 0.394, 0.396],
    [0.592, 0.603, 0.601],
    [0.889, 0.896, 0.895],
    [0.993, 0.995, 0.995],
    [1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000],
]
case_8 = [
    [0.847, 0.794, 0.792],
    [0.961, 0.933, 0.933],
    [0.999, 0.996, 0.996],
    [1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000],
]
case_2 = [
    [0.234, 0.098, 0.096],
    [0.352, 0.125, 0.127],
    [0.542, 0.250, 0.245],
    [0.856, 0.583, 0.580],
    [0.988, 0.943, 0.943],
    [1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000],
]
case_5 = [
    [0.427, 0.445, 0.442],
    [0.601, 0.614, 0.615],
    [0.828, 0.844, 0.842],
    [0.983, 0.988, 0.988],
    [1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000],
]
case_3 = [
    [0.687, 0.611, 0.605],
    [0.867, 0.805, 0.806],
    [0.980, 0.966, 0.965],
    [1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000],
]
case_9 = [
    [0.884, 0.829, 0.825],
    [0.972, 0.955, 0.955],
    [0.999, 0.998, 0.997],
    [1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000],
]
case_6 = [
    [0.169, 0.295, 0.292],
    [0.180, 0.355, 0.356],
    [0.211, 0.450, 0.447],
    [0.282, 0.571, 0.570],
    [0.385, 0.685, 0.684],
    [0.637, 0.823, 0.823],
    [0.868, 0.918, 0.917],
]
case_1 = [
    [0.047, 0.053, 0.051],
    [0.048, 0.047, 0.047],
    [0.049, 0.050, 0.049],
    [0.054, 0.047, 0.047],
    [0.053, 0.047, 0.046],
    [0.055, 0.046, 0.046],
    [0.055, 0.049, 0.047],
]
case_7 = [
    [0.293, 0.390, 0.387],
    [0.382, 0.507, 0.507],
    [0.521, 0.671, 0.669],
    [0.766, 0.853, 0.853],
    [0.951, 0.963, 0.963],
    [1.000, 0.998, 0.998],
    [1.000, 1.000, 1.000]
]

normal = [
    [0.050, 0.048, 0.046],
    [0.049, 0.051, 0.051],
    [0.048, 0.048, 0.047],
    [0.051, 0.048, 0.048],
    [0.050, 0.047, 0.047],
    [0.051, 0.052, 0.052],
    [0.054, 0.052, 0.050],
]

cases = [case_1, case_2, case_3, case_4, case_5, case_6, case_7, case_8, case_9]


def print_results():
    first_row_pattern = '\\multirow{{7}}{{*}}{{{0}}} & ${1}$ & ${2:.3f}$ & ${3:.3f}$ & ${4:.3f}$ & ' \
                        '\\multirow{{7}}{{*}}{{{5}}} & ${1}$ & ${6:.3f}$ & ${7:.3f}$ & ${8:.3f}$\\\\'
    row_pattern = '{0} & ${1}$ & ${2:.3f}$ & ${3:.3f}$ & ${4:.3f}$ & {5} & ${1}$ & ${6:.3f}$ & ${7:.3f}$ & ' \
                  '${8:.3f}$\\\\'
    case_last_row_pattern = row_pattern + '\\midrule'
    last_row_ever = row_pattern + '\\bottomrule'
    for idx in range(1, len(cases), 2):
        case1 = cases[idx]
        case2 = cases[idx + 1]
        for sample_size, result1, result2 in zip(sample_sizes, case1, case2):
            if sample_size == sample_sizes[0]:
                print(first_row_pattern.format(idx + 1, sample_size, *result1, idx + 2, *result2))
            elif sample_size == 1000:
                if idx == 7:
                    print(last_row_ever.format('', sample_size, *result1, '', *result2))
                else:
                    print(case_last_row_pattern.format('', sample_size, *result1, '', *result2))
            else:
                print(row_pattern.format('', sample_size, *result1, '', *result2))

    row_pattern = '{0} & ${1}$ & ${2:.3f}$ & ${3:.3f}$ & ${4:.3f}$\\\\'
    last_row_ever = row_pattern + '\\bottomrule'
    for sample_size, result1 in zip(sample_sizes, case_1):
        if sample_size == sample_sizes[0]:
            print('\\multirow{{7}}{{*}}{{{0}}} & ${1}$ & ${2:.3f}$ & ${3:.3f}$ & ${4:.3f}$ \\\\'
                  .format(1, sample_size, *result1))
        elif sample_size == sample_sizes[-1]:
            print(last_row_ever.format('', sample_size, *result1))
        else:
            print(row_pattern.format('', sample_size, *result1))


def plot_results():
    import matplotlib.pyplot as plt
    import numpy as np
    markersize = 10
    linewidth = 3.0
    n = 6
    for idx, case in enumerate(cases):
        case_array = np.array(case).T
        l1, = plt.semilogx(sample_sizes[0:n], case_array[0, 0:n], linestyle='--', marker='o', linewidth=linewidth,
                           markersize=markersize)
        l2, = plt.semilogx(sample_sizes[0:n], case_array[1, 0:n], linestyle='-', marker='D', linewidth=linewidth,
                           markersize=markersize)
        l3, = plt.semilogx(sample_sizes[0:n], case_array[2, 0:n], linestyle='-.', marker='s', linewidth=linewidth,
                           markersize=markersize)
        try:
            l4, = plt.semilogx(mcwilliams_sample_sizes, mcwilliams1990[idx], linestyle=':', marker='v',
                               linewidth=linewidth, markersize=markersize)
            l5, = plt.semilogx(others_sample_sizes, samawi2011[idx], linestyle='-', marker='p',
                               linewidth=linewidth, markersize=markersize)
            l6, = plt.semilogx(others_sample_sizes, modarres1998[idx], linestyle='--', marker='h',
                               linewidth=linewidth, markersize=markersize)
            l7, = plt.semilogx(others_sample_sizes, mira1999[idx], linestyle='-.', marker='<', linewidth=linewidth,
                               markersize=markersize)
            plt.legend((l1, l2, l3, l4, l5, l6, l7), ('O1', 'O2', 'dA70', 'McW90', 'S11', 'MG98', 'M99'),
                       loc='lower right', fancybox=True).get_frame().set_alpha(0.5)
            # plt.title('Case {}'.format(case_num))
        except TypeError:
            plt.legend((l1, l2, l3), ('O1', 'O2', 'dA70'), loc='lower right', fancybox=True).get_frame().set_alpha(0.5)

        miny = min(min(case[0]), mcwilliams1990[idx][0], samawi2011[idx][0], modarres1998[idx][0], mira1999[idx][0])
        maxy = max(max(case[n-1]), mcwilliams1990[idx][-1], samawi2011[idx][-1], modarres1998[idx][-1], mira1999[idx][-1])
        plt.ylim([miny - 0.05, maxy + 0.05])

        plt.xlim([sample_sizes[0] * 0.9, sample_sizes[n - 1] * 1.5])
        plt.xticks(sample_sizes[0:n], [str(e) for e in sample_sizes[0:n]])
        plt.grid(True)
        plt.xlabel('Sample Size')
        plt.ylabel('Empirical Power')
        plt.savefig('symmetry_case_{}.png'.format(idx+1), format='png', bbox_inches='tight')
        plt.close()


if __name__ == '__main__':
    plot_results()
