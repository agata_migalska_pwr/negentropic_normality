from operator import attrgetter
from itertools import repeat
from negentropic.pvalue import ACCEPT, REJECT
import functools


def order_p_values(p_values, key=attrgetter('p')):
    if is_sorted(p_values):
        return p_values, list(range(len(p_values)))
    else:
        indexes = sorted(range(len(p_values)), key=lambda k: key(p_values[k]))
        ordered = sorted(p_values, key=key)
        return ordered, indexes


def is_sorted(x, key=attrgetter('p')):
    return all([key(x[i]) <= key(x[i + 1]) for i in range(len(x) - 1)])


def sort_p_values(fcn):
    @functools.wraps(fcn)
    def wrapper(p_values, alpha):
        ordered, indexes = order_p_values(p_values)
        result = fcn(ordered, alpha)
        result_in_order = [result[i] for i in indexes]
        return result_in_order

    return wrapper


@sort_p_values
def bonferroni(p_values, alpha):
    """
    Bonferroni method
    :param p_values: vector of p-values
    :param alpha: family-wise significance level
    :return: vector of boolean values whether a null hypothesis at each i is rejected or not
    """
    alpha_i = alpha / len(p_values)
    return [REJECT if val.p <= alpha_i else ACCEPT for val in p_values]


@sort_p_values
def holm_bonferroni(p_values, alpha):
    """
    Holm, S. (1979). "A simple sequentially rejective multiple test procedure". Scandinavian Journal of Statistics.
    6 (2): 65–70
    :param p_values: vector of p-values
    :param alpha: family-wise significance level
    :return: vector of boolean values whether a null hypothesis at each i is rejected or not
    """
    i = 0
    n = len(p_values)
    result = list(repeat(ACCEPT, n))
    while i < n:
        if p_values[i].p > alpha / (n-i):
            # accept all
            return result
        # reject this one
        result[i] = REJECT
        i += 1
    return result


@sort_p_values
def fdr(p_values, p_rate):
    """
    Benjamini, Yoav; Hochberg, Yosef (1995). "Controlling the false discovery rate: a practical and powerful approach
    to multiple testing". Journal of the Royal Statistical Society, Series B. 57 (1): 289–300.
    :param p_values: vector of p-values
    :param p_rate: controlled rate of discovery
    :return: vector of boolean values whether a null hypothesis at each i is rejected or not
    """
    j = 0
    n = len(p_values)
    while j < n and p_values[j].p <= p_rate * (j + 1) / n:
        j += 1
    result = [REJECT if k < j else ACCEPT for k in range(n)]
    return result


def inverse_cumsum(n):
    result = 0
    for i in range(1, n+1):
        result += 1/i
    return result


@sort_p_values
def fdr_dependent(p_values, p_rate):
    """
    Benjamini, Yoav; Yekutieli, Daniel (2001). "The control of the false discovery rate in multiple testing under
    dependency" (PDF). Annals of Statistics. 29 (4): 1165–1188.
    :param p_values: vector of p-values
    :param p_rate: controlled rate of discovery
    :return: vector of boolean values whether a null hypothesis at each i is rejected or not
    """
    j = 0
    n = len(p_values)
    c = inverse_cumsum(n)
    multiplier = p_rate / (n * c)
    while j < n and p_values[j].p <= multiplier * (j + 1):
        j += 1
    result = [REJECT if k < j else ACCEPT for k in range(n)]
    return result
