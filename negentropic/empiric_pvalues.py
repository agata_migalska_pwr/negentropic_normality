import json
import os
import bisect


directory = 'ecdf'
file_format = 'ecdf_{}_{}.json'

def interpolate(x0, x1, y0, y1, k0):
    return x0 + (x1 - x0) * (k0 - y0) / (y1 - y0)


def p_value(fcn, k0, sample_size):
    with open(os.path.join(directory, file_format.format(type(fcn).__name__, sample_size)), 'r') as infile:
        a = json.load(infile)
        # a = a[::-1]
        idx = bisect.bisect(a, k0)
        assert type(idx) is int
        if 0 < idx < len(a):
            idx = interpolate(idx-1, idx, a[idx-1], a[idx], k0)
        if idx < 0:
            raise ValueError('what the hell?')
        p_val = min(idx + 1, len(a)) / len(a)
        assert 0.0 <= p_val <= 1.0
        return p_val


def quantile(fcn, p, sample_size):
    with open(os.path.join(directory, file_format.format(type(fcn).__name__, sample_size)), 'r') as infile:
        a = json.load(infile)
        n = len(a)  # nb of saved values, giving the precision
        idx = int(p*n) - 1
        return a[idx]




