from unittest import TestCase
import negentropic.multiple_hy_testing as mht
from negentropic.pvalue import PValue, REJECT, ACCEPT


class TestMultipleHypothesisTesting(TestCase):

    def setUp(self):
        pass

    def test_should_not_be_sorted(self):
        test_list = [PValue(p=0.7, k0=0.0), PValue(p=0.3, k0=0.0)]
        self.assertFalse(mht.is_sorted(test_list))

    def test_should_be_sorted(self):
        test_list = [PValue(p=0.3, k0=0.0), PValue(p=0.7, k0=0.0)]
        self.assertTrue(mht.is_sorted(test_list))

    def test_should_sort_if_not_sorted(self):
        test_list = [PValue(p=0.7, k0=0.0), PValue(p=0.3, k0=0.0)]
        ordered, indexes = mht.order_p_values(test_list)
        self.assertEqual(indexes[0], 1)
        self.assertEqual(indexes[1], 0)
        self.assertAlmostEqual(ordered[0], test_list[1])
        self.assertAlmostEqual(ordered[1], test_list[0])

    def test_should_not_sort_if_sorted(self):
        test_list = [PValue(p=0.3, k0=0.0), PValue(p=0.7, k0=0.0)]
        ordered, indexes = mht.order_p_values(test_list)
        self.assertEqual(indexes[0], 0)
        self.assertEqual(indexes[1], 1)
        self.assertAlmostEqual(ordered[0], test_list[0])
        self.assertAlmostEqual(ordered[1], test_list[1])

    def test_bonferroni_false_unsorted(self):
        result = mht.bonferroni([PValue(p=0.7, k0=0.0), PValue(p=0.3, k0=0.0)], 0.4)
        self.assertEqual(ACCEPT, all(result))

    def test_bonferroni_false_sorted(self):
        result = mht.bonferroni([PValue(p=0.3, k0=0.0), PValue(p=0.7, k0=0.0)], 0.4)
        self.assertEqual(ACCEPT, all(result))

    def test_bonferroni_mixed_unsorted(self):
        result = mht.bonferroni([PValue(p=0.3, k0=0.0), PValue(p=0.15, k0=0.0)], 0.4)
        self.assertEqual(REJECT, result[1])
        self.assertEqual(ACCEPT, result[0])

    def test_bonferroni_true_unsorted(self):
        result = mht.bonferroni([PValue(p=0.19, k0=0.0), PValue(p=0.15, k0=0.0)], 0.4)
        self.assertEqual(REJECT, all(result))

    def test_holm_acccepted_unsorted(self):
        result = mht.holm_bonferroni([PValue(p=0.7, k0=0.0), PValue(p=0.3, k0=0.0)], 0.4)
        self.assertEqual(ACCEPT, all(result))

    def test_holm_accepted_sorted(self):
        result = mht.holm_bonferroni([PValue(p=0.3, k0=0.0), PValue(p=0.7, k0=0.0)], 0.4)
        self.assertEqual(ACCEPT, all(result))

    def test_holm_mixed_unsorted(self):
        result = mht.holm_bonferroni([PValue(p=0.42, k0=0.0), PValue(p=0.15, k0=0.0)], 0.4)
        self.assertEqual(REJECT, result[1])
        self.assertEqual(ACCEPT, result[0])

    def test_holm_rejected_unsorted(self):
        result = mht.holm_bonferroni([PValue(p=0.3, k0=0.0), PValue(p=0.15, k0=0.0)], 0.4)
        self.assertEqual(REJECT, all(result))

    def test_fdr_acccepted_unsorted(self):
        result = mht.fdr([PValue(p=0.7, k0=0.0), PValue(p=0.3, k0=0.0)], 0.4)
        self.assertEqual(ACCEPT, all(result))

    def test_fdr_accepted_sorted(self):
        result = mht.fdr([PValue(p=0.3, k0=0.0), PValue(p=0.7, k0=0.0)], 0.4)
        self.assertEqual(ACCEPT, all(result))

    def test_fdr_mixed_unsorted(self):
        result = mht.fdr([PValue(p=0.42, k0=0.0), PValue(p=0.15, k0=0.0)], 0.4)
        # 0.15 shall be rejected, 0.42 accepted
        self.assertEqual(ACCEPT, result[0])
        self.assertEqual(REJECT, result[1])

    def test_fdr_rejected_unsorted(self):
        result = mht.fdr([PValue(p=0.3, k0=0.0), PValue(p=0.15, k0=0.0)], 0.4)
        # both shall be rejected
        self.assertEqual(REJECT, all(result))


    def test_fdr_dependent_acccepted_unsorted(self):
        result = mht.fdr_dependent([PValue(p=0.3, k0=0.0), PValue(p=0.14, k0=0.0)], 0.4)
        self.assertEqual(ACCEPT, all(result))

    def test_fdr_dependent_accepted_sorted(self):
        result = mht.fdr_dependent([PValue(p=0.14, k0=0.0), PValue(p=0.3, k0=0.0)], 0.4)
        self.assertEqual(ACCEPT, all(result))

    def test_fdr_dependent_mixed_unsorted(self):
        result = mht.fdr_dependent([PValue(p=0.3, k0=0.0), PValue(p=0.12, k0=0.0)], 0.4)
        # 0.12 shall be rejected, 0.3 accepted
        self.assertEqual(ACCEPT, result[0])
        self.assertEqual(REJECT, result[1])

    def test_fdr_dependent_rejected_unsorted(self):
        result = mht.fdr_dependent([PValue(p=0.26, k0=0.0), PValue(p=0.12, k0=0.0)], 0.4)
        # both shall be rejected
        self.assertEqual(REJECT, all(result))