import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
from negentropic.competitors import *
from collections import namedtuple
from negentropic import empiric_pvalues
from negentropic.pvalue import REJECT
from negentropic import multiple_hy_testing as mht
from negentropic.pvalue import PValue
from negentropic.distributions import *

SignificanceLevel = namedtuple('SignificanceLevel', ['p', 'K'])
markersize = 10
linewidth = 3.0


class NegentropyFunction:
    def __init__(self, mu, sigma_sq):
        self.mu = mu
        self.sigma_sq = sigma_sq

    def values(self, data):
        return self.get_value(data), self.mu, self.sigma_sq

    def get_value(self, data):
        raise NotImplementedError('Ask children')

    def cdf(self, stat, sample_size):
        return empiric_pvalues.p_value(self, stat, sample_size)


class OddFcn1(NegentropyFunction):
    def __init__(self):
        super().__init__(0.0, 1 / (3 * np.sqrt(3)))

    def get_value(self, data):
        return np.multiply(data, np.exp(np.divide(np.power(data, 2), -2)))


class OddFcn2(NegentropyFunction):
    def __init__(self):
        super().__init__(0, 1.32583)

    def get_value(self, data):
        return np.power(data, 3)


class EvenFcn1(NegentropyFunction):
    def __init__(self):
        super().__init__(- 1 / np.sqrt(2), 1 / np.sqrt(3) - 0.5)

    def get_value(self, data):
        return np.multiply(-1, np.exp(np.divide(np.power(data, 2), -2)))


class EvenFcn2(NegentropyFunction):
    def __init__(self):
        super().__init__(0.3745672074, 0.1897674493)

    def get_value(self, data):
        return np.log(np.cosh(data))


class EvenFcn3(NegentropyFunction):
    def __init__(self):
        super().__init__(3, 96)

    def get_value(self, data):
        return np.power(data, 4)


def test_statistic(data, approx_fcn):
    # reshape to a vector
    """
    :type data: np.ndarray
    :type approx_fcn: NegentropyFunction
    """
    data = np.ndarray.flatten(data)
    # zero-mean
    data = np.subtract(data, np.mean(data))
    # unit variance
    data = np.divide(data, np.std(data))

    g, mu, sigma_sq = approx_fcn.values(data)
    n = np.size(data)
    k0 = (np.sum(g) - n * mu) ** 2 / sigma_sq / n
    return k0


def test_statistic_p_val(data, approx_fcn):
    # pval_chisq = 1 - stats.chi2.cdf(k0, 1)
    k0 = test_statistic(data, approx_fcn)
    sample_size = len(data)
    p_value = 1 - approx_fcn.cdf(k0, sample_size)
    return PValue(k0=k0, p=p_value)


alpha_verification_lambdas_matrix = {'sym1': [0.0, 2.0, 1.0, 1.0],
                                     'sym2': [0.0, 0.197454, 0.134915, 0.134915],
                                     'sym3': [0.0, -1.0, -0.08, -0.08],
                                     'sym4': [0.0, -0.397912, -0.16, -0.16],
                                     'sym5': [0.0, -1.0, -0.24, -0.24],
                                     'sym6': [0.0, -5.11256, -1.0, -1.0]}

# these two examples have kurtosis 3 but their peaks are different than those of a normal distribution
# this demonstrates that kurtosis has nothing to do with peakedness. it is interesting to find out what the test we
# propose says about that
# l1 = 0, lambda2 = lambda3 = lambda4
disproved_peakedness_lambdas = {'peakedness1': [0.0, 0.135, 0.135, 0.135],
                                'peakedness2': [0.0, 5.2, 5.2, 5.2]}

lambdas_matrix = {'case_1': [0.0, 0.197454, 0.134915, 0.134915],
                  'case_2': [0.0, 1.0, 1.4, 0.25],
                  'case_3': [0.0, 1.0, 0.00007, 0.1],
                  'case_4': [3.586508, 0.04306, 0.025213, 0.094029],
                  'case_5': [0.0, -1.0, -0.0075, -0.03],
                  'case_6': [-0.116734, -0.351663, -0.13, -0.16],
                  'case_7': [0.0, -1.0, -0.1, -0.18],
                  'case_8': [0.0, -1.0, -0.001, -0.13],
                  'case_9': [0.0, -1.0, -0.0001, -0.17]}

skewness_matrix = {'case1': 0.0,
                   'case2': 0.5,
                   'case3': 1.5,
                   'case4': 0.9,
                   'case5': 1.5,
                   'case6': 0.8,
                   'case7': 2.0,
                   'case8': 3.16,
                   'case9': 3.88}

kurtosis_matrix = {'case1': 3.0,
                   'case2': 2.2,
                   'case3': 5.8,
                   'case4': 4.2,
                   'case5': 7.5,
                   'case6': 11.4,
                   'case7': 21.2,
                   'case8': 23.8,
                   'case9': 40.7}

sample_sizes = [20, 30, 50, 100, 200, 500, 1000]
num_samples = 10000


def monte_carlo_tailedness(generator, case_name, alpha=0.05):
    print('{} = ['.format(case_name))
    even3_powers = []
    even2_powers = []
    even1_powers = []
    kurtosis_powers = []
    even_fcn1 = EvenFcn1()
    even_fcn2 = EvenFcn2()
    even_fcn3 = EvenFcn3()
    for sample_size in sample_sizes:
        even3_rejected = 0
        even2_rejected = 0
        even1_rejected = 0
        kurtosis_rejected = 0
        for _ in range(num_samples):
            sample = generator.sample(sample_size)
            kurt = PValue(*stats.mstats.kurtosistest(sample))
            neg_even1 = test_statistic_p_val(sample, even_fcn1)
            neg_even2 = test_statistic_p_val(sample, even_fcn2)
            neg_even3 = test_statistic_p_val(sample, even_fcn3)
            if neg_even1.p <= alpha:
                even1_rejected += 1
            if neg_even2.p <= alpha:
                even2_rejected += 1
            if kurt.p <= alpha:
                kurtosis_rejected += 1
            if neg_even3.p <= alpha:
                even3_rejected += 1

        even1_powers.append(even1_rejected / num_samples)
        even2_powers.append(even2_rejected / num_samples)
        even3_powers.append(even3_rejected / num_samples)
        kurtosis_powers.append(kurtosis_rejected / num_samples)
        # print('{} & {} & & {} & {} & {} & {} \\\\'
        #      .format(case_num, sample_size, kurtosis_rejected, even1_rejected, even2_rejected, even3_rejected))
        print('[{0:.3f}, {1:.3f}, {2:.3f}, {3:.3f}],'.format(even1_powers[-1], even2_powers[-1], even3_powers[-1],
                                                             kurtosis_powers[-1]))
    print(']')

    l1, = plt.semilogx(sample_sizes, even1_powers, linestyle='--', marker='o', linewidth=linewidth,
                       markersize=markersize)
    l2, = plt.semilogx(sample_sizes, even2_powers, linestyle='-', marker='D', linewidth=linewidth,
                       markersize=markersize)
    l3, = plt.semilogx(sample_sizes, even3_powers, linestyle='-.', marker='s', linewidth=linewidth,
                       markersize=markersize)
    l4, = plt.semilogx(sample_sizes, kurtosis_powers, linestyle=':', marker='v', linewidth=linewidth,
                       markersize=markersize)
    plt.legend((l1, l2, l3, l4), ('E1', 'E2', 'E3', 'KT'), loc='lower right', fancybox=True) \
        .get_frame().set_alpha(0.5)
    plt.xlabel('Sample Size')
    plt.ylabel('Empirical Power')
    plt.ylim([0.0, 1.1])
    plt.grid(True)
    plt.savefig('tailedness_{}.png'.format(case_name), format='png')
    plt.close()


def monte_carlo_symmetry(generator, case_name, alpha=0.05):
    """

    :type generator: Distribution
    :type case_name: str
    :type alpha: float
    """

    case_num = case_num_from_case_name(case_name)

    print('{} = ['.format(case_name))
    neg_odd1_powers = []
    skewtest_powers = []
    neg_odd2_powers = []
    odd_fcn1 = OddFcn1()
    odd_fcn2 = OddFcn2()

    for sample_size in sample_sizes:
        neg_odd1_rejected = 0
        neg_odd2_rejected = 0
        skewtest_rejected = 0
        for _ in range(num_samples):
            sample = generator.sample(sample_size)
            skew = PValue(*stats.mstats.skewtest(sample))
            neg_odd1 = test_statistic_p_val(sample, odd_fcn1)
            neg_odd2 = test_statistic_p_val(sample, odd_fcn2)
            if neg_odd1.p <= alpha:
                neg_odd1_rejected += 1
            if skew.p <= alpha:
                skewtest_rejected += 1
            if neg_odd2.p <= alpha:
                neg_odd2_rejected += 1

        neg_odd1_powers.append(neg_odd1_rejected / num_samples)
        neg_odd2_powers.append(neg_odd2_rejected / num_samples)
        skewtest_powers.append(skewtest_rejected / num_samples)

        print('[{0:.3f}, {1:.3f}, {2:.3f}],'.format(neg_odd1_powers[-1], neg_odd2_powers[-1], skewtest_powers[-1]))
    print(']')

    l1, = plt.semilogx(sample_sizes, neg_odd1_powers, linestyle='--', marker='o', linewidth=linewidth,
                       markersize=markersize)
    l2, = plt.semilogx(sample_sizes, neg_odd2_powers, linestyle='-', marker='D', linewidth=linewidth,
                       markersize=markersize)
    l3, = plt.semilogx(sample_sizes, skewtest_powers, linestyle='-.', marker='s', linewidth=linewidth,
                       markersize=markersize)
    try:
        l4, = plt.semilogx(mcwilliams_sample_sizes, mcwilliams1990[case_num - 1], linestyle=':', marker='v',
                           linewidth=linewidth, markersize=markersize)
        l5, = plt.semilogx(others_sample_sizes, samawi2011[case_num - 1], linestyle='-', marker='p',
                           linewidth=linewidth, markersize=markersize)
        l6, = plt.semilogx(others_sample_sizes, modarres1998[case_num - 1], linestyle='--', marker='h',
                           linewidth=linewidth, markersize=markersize)
        l7, = plt.semilogx(others_sample_sizes, mira1999[case_num - 1], linestyle='-.', marker='<', linewidth=linewidth,
                           markersize=markersize)
        plt.legend((l1, l2, l3, l4, l5, l6, l7), ('O1', 'O2', 'dA70', 'McW90', 'S11', 'MG98', 'M99'), loc='lower right',
                   fancybox=True).get_frame().set_alpha(0.5)
        # plt.title('Case {}'.format(case_num))
    except TypeError:
        print('Case num was not given I believe... {}'.format(case_num))
        plt.legend((l1, l2, l3), ('O1', 'O2', 'dA70'), loc='lower right', fancybox=True).get_frame().set_alpha(0.5)

    plt.ylim([0.0, 1.1])
    plt.grid(True)
    plt.xlabel('Sample Size')
    plt.ylabel('Empirical Power')
    plt.savefig('symmetry_{}.png'.format(case_name), format='png')
    plt.close()


def monte_carlo_normality(generator, case_name, mht_method=mht.holm_bonferroni, alpha=0.05):

    print('{} = ['.format(case_name))

    wilk_powers = []
    odd1_even1_powers = []
    odd1_even2_powers = []
    odd1_even3_powers = []
    odd2_even1_powers = []
    odd2_even2_powers = []
    odd2_even3_powers = []
    jarque_bera_powers = []
    dagostino_powers = []

    odd_fcn1 = OddFcn1()
    odd_fcn2 = OddFcn2()
    even_fcn1 = EvenFcn1()
    even_fcn2 = EvenFcn2()
    even_fcn3 = EvenFcn3()

    for sample_size in sample_sizes:
        wilk_rejected = 0
        odd1_even1_rejected = 0
        odd1_even2_rejected = 0
        odd1_even3_rejected = 0
        odd2_even1_rejected = 0
        odd2_even2_rejected = 0
        odd2_even3_rejected = 0
        jarque_bera_rejected = 0
        dagostino_rejected = 0
        for _ in range(num_samples):
            sample = generator.sample(sample_size)
            wilk, p_wilk = stats.shapiro(sample)
            jb, p_jb = stats.jarque_bera(sample)
            da, p_da = stats.mstats.normaltest(sample)
            p_odd1 = test_statistic_p_val(sample, odd_fcn1)
            p_odd2 = test_statistic_p_val(sample, odd_fcn2)

            p_even1 = test_statistic_p_val(sample, even_fcn1)
            p_even2 = test_statistic_p_val(sample, even_fcn2)
            p_even3 = test_statistic_p_val(sample, even_fcn3)
            if p_wilk <= alpha:
                wilk_rejected += 1
            if any(mht_method([p_odd1, p_even1], alpha)) is REJECT:
                odd1_even1_rejected += 1
            if any(mht_method([p_odd1, p_even2], alpha)) is REJECT:
                odd1_even2_rejected += 1
            if any(mht_method([p_odd1, p_even3], alpha)) is REJECT:
                odd1_even3_rejected += 1
            if any(mht_method([p_odd2, p_even1], alpha)) is REJECT:
                odd2_even1_rejected += 1
            if any(mht_method([p_odd2, p_even2], alpha)) is REJECT:
                odd2_even2_rejected += 1
            if any(mht_method([p_odd2, p_even3], alpha)) is REJECT:
                odd2_even3_rejected += 1
            if p_jb <= alpha:
                jarque_bera_rejected += 1
            if p_da <= alpha:
               dagostino_rejected += 1

        wilk_powers.append(wilk_rejected / num_samples)
        odd1_even1_powers.append(odd1_even1_rejected / num_samples)
        odd1_even2_powers.append(odd1_even2_rejected / num_samples)
        odd1_even3_powers.append(odd1_even3_rejected / num_samples)
        odd2_even1_powers.append(odd2_even1_rejected / num_samples)
        odd2_even2_powers.append(odd2_even2_rejected / num_samples)
        odd2_even3_powers.append(odd2_even3_rejected / num_samples)
        jarque_bera_powers.append(jarque_bera_rejected / num_samples)
        dagostino_powers.append(dagostino_rejected / num_samples)

        # print('[{0:.3f}, {1:.3f}, {2:.3f}, {3:.3f}, {4:.3f}, {5:.3f}, {6:.3f}],'
        #      .format(odd1_even1_powers[-1], odd1_even2_powers[-1], odd1_even3_powers[-1], odd2_even1_powers[-1],
        #              odd2_even2_powers[-1], odd2_even3_powers[-1], wilk_powers[-1]))
        print('[{0:.3f}, {1:.3f}, {2:.3f}, {3:.3f}, {4:.3f}, {5:.3f}, {6:.3f}, {7:.3f}, {8:.3f}],'
              .format(odd1_even1_powers[-1], odd1_even2_powers[-1], odd1_even3_powers[-1], odd2_even1_powers[-1],
                      odd2_even2_powers[-1], odd2_even3_powers[-1], wilk_powers[-1], jarque_bera_powers[-1],
                      dagostino_powers[-1]))
    print(']')
    """
    l1, = plt.semilogx(sample_sizes, odd1_even1_powers, linestyle='-', marker='o', linewidth=linewidth,
                       markersize=markersize)
    l2, = plt.semilogx(sample_sizes, odd1_even2_powers, linestyle='--', marker='D', linewidth=linewidth,
                       markersize=markersize)
    l3, = plt.semilogx(sample_sizes, odd1_even3_powers, linestyle='-.', marker='s', linewidth=linewidth,
                       markersize=markersize)
    l4, = plt.semilogx(sample_sizes, odd2_even1_powers, linestyle='-', marker='v', linewidth=linewidth,
                       markersize=markersize)
    l5, = plt.semilogx(sample_sizes, odd2_even2_powers, linestyle='--', marker='p', linewidth=linewidth,
                       markersize=markersize)
    l6, = plt.semilogx(sample_sizes, odd2_even3_powers, linestyle='-.', marker='h', linewidth=linewidth,
                       markersize=markersize)
    l7, = plt.semilogx(sample_sizes, wilk_powers, linestyle='-', marker='<', linewidth=linewidth,
                       markersize=markersize)

    l8, = plt.semilogx(sample_sizes, jarque_bera_powers, linestyle='-', marker='d', linewidth=linewidth,
                       markersize=markersize)
    l9, = plt.semilogx(sample_sizes, dagostino_powers, linestyle=':', marker='>', linewidth=linewidth,
                       markersize=markersize)

    plt.legend((l1, l2, l3, l4, l5, l6, l7, l8, l9), ('O1+E1', 'O1+E2', 'O1+E3', 'O2+E1', 'O2+E2', 'O2+E3', 'SW', 'JB',
                                                      'd\'A'), loc='lower right', fancybox=True) \
        .get_frame().set_alpha(0.5)

    plt.legend((l1, l2, l3, l4, l5, l6, l7), ('O1+E1', 'O1+E2', 'O1+E3', 'O2+E1', 'O2+E2', 'O2+E3', 'SW'),
               loc='lower right', fancybox=True).get_frame().set_alpha(0.5)
    plt.ylim([0.0, 1.1])
    plt.grid(True)
    plt.savefig('normality_{}_{}.png'.format(case_name, mht_method.__name__), format='png')
    plt.close()
    """


def benchmark(monte_carlo, generator, case_name, *args):
    generator.plot(case_name)
    monte_carlo(generator, case_name, *args)


def case_num_from_case_name(case_name):
    case_num = None
    if case_name.startswith('case'):
        case_num = int(case_name[-1:])
        assert 0 < case_num <= 9
    return case_num


def benchmark_generalized_lambdas(monte_carlo, lambdas, *args):
    print(monte_carlo)
    print(*args)
    for case_name, lambdas in lambdas.items():
        generator = GeneralizedLambdaFamily(*lambdas)
        benchmark(monte_carlo, generator, case_name, *args)


if __name__ == '__main__':
    benchmark_generalized_lambdas(monte_carlo_normality, lambdas_matrix, mht.bonferroni)
    benchmark_generalized_lambdas(monte_carlo_normality, lambdas_matrix, mht.fdr)
    """
    mht_methods = [mht.bonferroni, mht.holm_bonferroni, mht.fdr]  # , mht.fdr_dependent]
    for mht_method in mht_methods:
        benchmark_generalized_lambdas(monte_carlo_normality, lambdas_matrix, mht_method)
    benchmark_generalized_lambdas(monte_carlo_tailedness, lambdas_matrix)
    benchmark_generalized_lambdas(monte_carlo_symmetry, lambdas_matrix)
    # benchmark_generalized_lambdas(monte_carlo_normality, lambdas_matrix)
    # benchmark_generalized_lambdas(monte_carlo_symmetry, alpha_verification_lambdas_matrix)
    # monte_carlo_symmetry(StandardNormalDistribution(), 'normal1')
    # monte_carlo_symmetry(UniformDistribution(1, 5), 'uniform1')


    fcns = [OddFcn1(), OddFcn2(), EvenFcn1(), EvenFcn2(), EvenFcn3()]
    for fcn in fcns:
        for case in lambdas_matrix.keys():
            generator = GeneralizedLambdaFamily(*lambdas_matrix[case])
            for sample_size in sample_sizes:
                sample = generator.sample(sample_size)
                test_statistic(sample, fcn)
    """