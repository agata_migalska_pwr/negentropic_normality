from negentropic import negentropic_test
from negentropic import empiric_pvalues

functions = [negentropic_test.OddFcn1(),
             negentropic_test.OddFcn1(),
             negentropic_test.EvenFcn1(),
             negentropic_test.EvenFcn2(),
             negentropic_test.EvenFcn3()]
pattern = '{0} & {1:.4f} & {2:.4f} & {3:.4f} & {4:.4f} & {5:.4f}\\\\'
print('\\textbf{Sample Size} & $\\mathbf{O1}$ & $\\mathbf{O2}$ & $\\mathbf{E1}$ & $\\mathbf{E2}$ & $\\mathbf{E3}$ '
      '\\\\\\midrule')
p = 0.95
for sample_size in negentropic_test.sample_sizes:
    vals = list()
    for fun in functions:
        vals.append(empiric_pvalues.quantile(fun, p, sample_size))
    print(pattern.format(sample_size, *vals))
print('\\bottomrule')
