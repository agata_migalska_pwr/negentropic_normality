from negentropic import negentropic_test as nt
from sortedcontainers import SortedList
from operator import attrgetter
from negentropic.pvalue import PValue


alpha = 0.05
num_samples = 10000
num_pvalues = int(alpha * num_samples * 1.1)
threshold = int(alpha * num_samples)


sample_sizes = nt.sample_sizes

# normal_lambdas = nt.lambdas_matrix['case_1']
# nt.GeneralizedLambdaFamily(*normal_lambdas)
generator = nt.StandardNormalDistribution()


def test_result(sample1, function):
    return PValue(*nt.test_statistic_p_val(sample1, function))


def evaluate(sample1, function, p_values):
    p_value = test_result(sample1, function)
    if len(p_values) < num_pvalues:
        p_values.add(p_value)
    elif p_value.p < p_values[-1].p:
        del p_values[-1]
        p_values.add(p_value)


def determine_asymptotic_alpha(function):
    current_pvalues = SortedList(key=attrgetter('p'))
    for sample_size in sample_sizes:
        for _ in range(num_samples):
            sample = generator.sample(sample_size)
            evaluate(sample, function, current_pvalues)
        winner = current_pvalues[threshold]
        print('Sample size: {}, Asymptotic alpha={} for k0 = {}, Sanity check: {}'
              .format(sample_size, winner.p, winner.k0, len(current_pvalues)))
        current_pvalues.clear()


def run(funs_to_test):
    for fun in funs_to_test:
        print('Function under test: {}'.format(fun))
        determine_asymptotic_alpha(fun)


if __name__ == '__main__':
    functions_to_test = [nt.odd_fcn1, nt.odd_fcn2, nt.even_fcn1, nt.even_fcn2, nt.even_fcn3]
    run(functions_to_test)
