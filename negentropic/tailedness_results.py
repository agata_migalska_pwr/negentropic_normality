case_4 = [
    [0.110, 0.120, 0.153, 0.132],
    [0.139, 0.160, 0.213, 0.177],
    [0.175, 0.208, 0.285, 0.233],
    [0.264, 0.331, 0.434, 0.363],
    [0.422, 0.496, 0.635, 0.565],
    [0.714, 0.808, 0.910, 0.888],
    [0.935, 0.968, 0.993, 0.991],
]
case_8 = [
    [0.425, 0.451, 0.516, 0.478],
    [0.560, 0.593, 0.665, 0.620],
    [0.752, 0.785, 0.848, 0.807],
    [0.937, 0.952, 0.974, 0.962],
    [0.998, 0.999, 1.000, 1.000],
    [1.000, 1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000, 1.000],
]
case_2 = [
    [0.184, 0.169, 0.117, 0.159],
    [0.260, 0.247, 0.132, 0.249],
    [0.418, 0.383, 0.225, 0.382],
    [0.720, 0.706, 0.449, 0.641],
    [0.944, 0.929, 0.791, 0.875],
    [1.000, 1.000, 0.996, 0.999],
    [1.000, 1.000, 1.000, 1.000],
]
case_5 = [
    [0.215, 0.235, 0.283, 0.251],
    [0.292, 0.328, 0.394, 0.349],
    [0.414, 0.451, 0.541, 0.474],
    [0.649, 0.711, 0.782, 0.731],
    [0.881, 0.910, 0.952, 0.931],
    [0.995, 0.998, 0.999, 0.999],
    [1.000, 1.000, 1.000, 1.000],
]
case_3 = [
    [0.214, 0.233, 0.298, 0.268],
    [0.267, 0.306, 0.393, 0.339],
    [0.376, 0.431, 0.559, 0.485],
    [0.565, 0.648, 0.780, 0.717],
    [0.797, 0.857, 0.944, 0.919],
    [0.984, 0.994, 1.000, 0.999],
    [1.000, 1.000, 1.000, 1.000],
]
case_9 = [
    [0.459, 0.485, 0.550, 0.515],
    [0.605, 0.639, 0.704, 0.657],
    [0.806, 0.833, 0.884, 0.851],
    [0.963, 0.974, 0.986, 0.979],
    [1.000, 1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000, 1.000],
]
case_6 = [
    [0.262, 0.272, 0.301, 0.260],
    [0.364, 0.381, 0.405, 0.349],
    [0.568, 0.585, 0.607, 0.525],
    [0.822, 0.837, 0.832, 0.778],
    [0.980, 0.980, 0.976, 0.964],
    [1.000, 1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000, 1.000],
]
case_1 = [
    [0.047, 0.046, 0.044, 0.044],
    [0.046, 0.047, 0.049, 0.048],
    [0.046, 0.045, 0.044, 0.047],
    [0.048, 0.051, 0.042, 0.044],
    [0.051, 0.048, 0.043, 0.045],
    [0.045, 0.046, 0.034, 0.037],
    [0.045, 0.043, 0.032, 0.034],
]
case_7 = [
    [0.287, 0.300, 0.334, 0.294],
    [0.412, 0.433, 0.463, 0.409],
    [0.596, 0.617, 0.648, 0.580],
    [0.857, 0.875, 0.877, 0.839],
    [0.981, 0.983, 0.984, 0.977],
    [1.000, 1.000, 1.000, 1.000],
    [1.000, 1.000, 1.000, 1.000],
]

normal = [
    [0.050, 0.050, 0.050, 0.046],
    [0.048, 0.050, 0.050, 0.047],
    [0.050, 0.048, 0.050, 0.053],
    [0.047, 0.052, 0.049, 0.054],
    [0.050, 0.051, 0.053, 0.053],
    [0.051, 0.052, 0.049, 0.053],
    [0.048, 0.049, 0.048, 0.052],
]

cases = [case_1, case_2, case_3, case_4, case_5, case_6, case_7, case_8, case_9]
sample_sizes = [20, 30, 50, 100, 200, 500, 1000]


def print_results():
    first_row_pattern = '\\multirow{{7}}{{*}}{{{0}}} & ${1}$ & ${2:.3f}$ & ${3:.3f}$ & ${4:.3f}$ & ${5:.3f}$ & ' \
                        '\\multirow{{7}}{{*}}{{{6}}} & ${1}$ & ${7:.3f}$ & ${8:.3f}$ & ${9:.3f}$ & ${10:.3f}$\\\\'
    row_pattern = '{0} & ${1}$ & ${2:.3f}$ & ${3:.3f}$ & ${4:.3f}$ & ${5:.3f}$ & {6} & ${1}$ & ${7:.3f}$ & ${8:.3f}$ &' \
                  ' ${9:.3f}$ & ${10:.3f}$\\\\'
    case_last_row_pattern = row_pattern + '\\midrule'
    last_row_ever = row_pattern + '\\bottomrule'
    for idx in range(1, len(cases), 2):
        case1 = cases[idx]
        case2 = cases[idx + 1]
        for sample_size, result1, result2 in zip(sample_sizes, case1, case2):
            if sample_size == sample_sizes[0]:
                print(first_row_pattern.format(idx + 1, sample_size, *result1, idx + 2, *result2))
            elif sample_size == 1000:
                if idx == 7:
                    print(last_row_ever.format('', sample_size, *result1, '', *result2))
                else:
                    print(case_last_row_pattern.format('', sample_size, *result1, '', *result2))
            else:
                print(row_pattern.format('', sample_size, *result1, '', *result2))

    row_pattern = '{0} & ${1}$ & ${2:.3f}$ & ${3:.3f}$ & ${4:.3f}$ & ${5:.3f}$\\\\'
    last_row_ever = row_pattern + '\\bottomrule'
    for sample_size, result1 in zip(sample_sizes, normal):
        if sample_size == sample_sizes[0]:
            print('\\multirow{{7}}{{*}}{{{0}}} & ${1}$ & ${2:.3f}$ & ${3:.3f}$ & ${4:.3f}$ & ${5:.3f}$ \\\\'
                  .format(1, sample_size, *result1))
        elif sample_size == sample_sizes[-1]:
            print(last_row_ever.format('', sample_size, *result1))
        else:
            print(row_pattern.format('', sample_size, *result1))


def plot_results():
    import matplotlib.pyplot as plt
    import numpy as np
    markersize = 10
    linewidth = 3.0
    n = 6
    for idx, case in enumerate(cases):
        case_array = np.array(case).T
        l1, = plt.semilogx(sample_sizes[0:n], case_array[0, 0:n], linestyle='--', marker='o', linewidth=linewidth,
                           markersize=markersize)
        l2, = plt.semilogx(sample_sizes[0:n], case_array[1, 0:n], linestyle='-', marker='D', linewidth=linewidth,
                           markersize=markersize)
        l3, = plt.semilogx(sample_sizes[0:n], case_array[2, 0:n], linestyle='-.', marker='s', linewidth=linewidth,
                           markersize=markersize)
        l4, = plt.semilogx(sample_sizes[0:n], case_array[3, 0:n], linestyle=':', marker='v', linewidth=linewidth,
                           markersize=markersize)
        plt.legend((l1, l2, l3, l4), ('E1', 'E2', 'E3', 'KT'), loc='lower right', fancybox=True) \
            .get_frame().set_alpha(0.5)
        plt.xlabel('Sample Size')
        plt.ylabel('Empirical Power')
        miny = min(case[0])
        maxy = max(case[n - 1])
        plt.ylim([miny - 0.05, maxy + 0.05])
        plt.xlim([sample_sizes[0]*0.9, sample_sizes[n-1]*1.5])
        plt.xticks(sample_sizes[0:n], [str(e) for e in sample_sizes[0:n]])
        plt.grid(True)
        plt.savefig('tailedness_case_{}.eps'.format(idx+1), format='eps',
                    bbox_inches='tight', dpi=300)
        plt.close()


if __name__ == '__main__':
    plot_results()
