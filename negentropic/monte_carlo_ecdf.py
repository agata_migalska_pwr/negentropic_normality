import sys
from sortedcontainers import SortedList
from negentropic import negentropic_test as nt
from negentropic.pvalue import PValue
from operator import attrgetter
import json
import numpy as np
import matplotlib.pyplot as plt
import os


num_samples = 20000
num_pvalues = num_samples
num_steps = 1000
sample_sizes = nt.sample_sizes
generator = nt.StandardNormalDistribution()
directory = 'ecdf'


def test_result(sample1, function):
    #return PValue(*nt.test_statistic_p_val(sample1, function))
    return nt.test_statistic(sample1, function)


def evaluate(sample1, function, test_stats):
    test_stats.add(test_result(sample1, function))


def plot_ecdf(values, steps, legend_nb):
    y = np.arange(0.0, 1.0, 1.0 / steps)
    leg = list()
    for vals in values:
        l, = plt.plot(vals, y)
        leg.append(l)
    plt.legend(leg, legend_nb)
    plt.show()


def ensure_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def determine_asymptotic_alpha(function):
    current_teststats = SortedList()
    all_quantiles = list()
    for sample_size in sample_sizes:
        for _ in range(num_samples):
            sample = generator.sample(sample_size)
            evaluate(sample, function, current_teststats)
        single_range = num_pvalues//num_steps
        quantiles = [current_teststats[i] for i in range(single_range-1, num_pvalues, single_range)]
        assert len(quantiles) == num_steps
        ensure_dir(directory)
        with open(os.path.join(directory, 'ecdf_{}_{}.json'.format(type(function).__name__, sample_size)), 'w') as outfile:
            json.dump(quantiles, outfile)
        current_teststats.clear()
        all_quantiles.append(quantiles)
    # plot_ecdf(all_quantiles, num_steps, sample_sizes)


def construct_ecdf(function):
    # it has to be run for each function -- so 5 times
    """
    test_stats = SortedList()
    list_size = sys.getsizeof(test_stats)
    float_size = sys.getsizeof(0.0)
    memory = num_pvalues * float_size + list_size
    what = input('This process will consume {} B of memory. Press n to cancel or y to calculate anyway.'.format(memory))
    if what is 'n':
        return
    """
    print('Please be patient, it will take a few minutes.')
    determine_asymptotic_alpha(function)


if __name__ == '__main__':
    functions_to_test = [nt.OddFcn1(), nt.OddFcn2(), nt.EvenFcn1(), nt.EvenFcn2(), nt.EvenFcn3()]
    for fcn in functions_to_test:
        construct_ecdf(fcn)
