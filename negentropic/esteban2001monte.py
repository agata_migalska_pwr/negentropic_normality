from negentropic import negentropic_test
from negentropic.distributions import student, standard_logistic, my_cauchy, standard_double_exp, gumbel, exponential, \
    lognormal, my_gamma, weibull, my_uniform, my_beta, GenericDistribution
import sys


def one_test(distrib, case_name):
    try:
        negentropic_test.monte_carlo_normality(GenericDistribution(distrib), case_name)
    except:
        print("Unexpected error: {}".format(sys.exc_info()[0]))


def group_benchmark(name, group):
    print(name)
    for name, generator in group.items():
        one_test(generator, name)

if __name__ == '__main__':

    groupI = {'t(1)': student(1),
              't(3)': student(3),
              'standard logistic': standard_logistic(),
              'cauchy(0, 2)': my_cauchy(0,2),
              'cauchy(0, 0.5)': my_cauchy(0, 0.5),
              'standard double exponential': standard_double_exp()}

    group_benchmark('Group I', groupI)

    groupII = {'gumbel(0, 1)': gumbel(0, 1),
               'gumbel(0, 2)': gumbel(0, 2),
               'gumbel(0, 0.5)': gumbel(0, 0.5)}

    group_benchmark('Group II', groupII)

    groupIII = {'exponential': exponential(),
                'gamma(2)': my_gamma(2),
                'gamma(0.5)': my_gamma(0.5),
                'lognormal(1)': lognormal(1),
                'lognormal(2)': lognormal(2),
                'lognormal(0.5)': lognormal(0.5),
                'weibull(0.5)': weibull(0.5),
                'weibull(2)': weibull(2)}

    group_benchmark('Group III', groupIII)

    groupIV = {'uniform': my_uniform(),
               'beta(2,2)': my_beta(2, 2),
               'beta(0.5, 0.5)': my_beta(0.5, 0.5),
               'beta(3, 1.5)': my_beta(3, 1.5),
               'beta(2, 1)': my_beta(2, 1)}

    group_benchmark('Group IV', groupIV)




