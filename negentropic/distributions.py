import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import t, cauchy, laplace, logistic, gumbel_r, weibull_min, uniform, beta, lognorm, gamma, expon
import functools


class Distribution:
    def __init__(self):
        self.skewness = None
        self.kurtosis = None

    def plot(self, case_name):
        delta = 0.001
        vector_u = np.arange(delta, 1.0, delta)
        delta_u = vector_u[1] - vector_u[0]
        x = []
        fx = []
        for idx, u in np.ndenumerate(vector_u):
            if idx[0] == 0:
                x.append(self.inverse_cdf(u))
                fx.append(u)
                continue
            x0 = x[-1]  # previous element
            x1 = self.inverse_cdf(u)
            x.append(x1)
            f0 = fx[-1]  # previous function value
            f1 = 2 * delta_u / (x1 - x0) - f0
            fx.append(f1)

        new_x = []
        new_fx = []
        for i in range(len(x) - 1):
            new_x.append((x[i + 1] + x[i]) / 2)
            new_fx.append((fx[i + 1] + fx[i]) / 2)
        plt.plot(new_x, new_fx, linestyle='-', linewidth=2)
        # plt.title(self.get_name(case_name))
        plt.savefig('pdf_{}.png'.format(case_name), format='png')
        plt.close()

    def sample(self, sample_size):
        u = np.random.uniform(size=sample_size)
        return self.inverse_cdf(u)

    def inverse_cdf(self, u):
        pass


class GeneralizedLambdaFamily(Distribution):
    def __init__(self, l1, l2, l3, l4):
        self.l1 = l1
        self.l2 = l2
        self.l3 = l3
        self.l4 = l4

    def inverse_cdf(self, u):
        return np.add(self.l1, np.divide((np.power(u, self.l3) - np.power(1 - u, self.l4)), self.l2))

    def get_name(self, case_name):
        return 'Case {}'.format(case_name[-1:])


class UniformDistribution(Distribution):
    def __init__(self, a, b):
        self.a = a
        self.b = b
        self.kurtosis = 1.8
        self.skewness = 0.0

    def inverse_cdf(self, u):
        return np.add(self.a, np.multiply(u, self.b - self.a))

    def get_name(self, case_name):
        return 'Continuous Uniform Distribution on [{},{}]'.format(self.a, self.b)


class StandardNormalDistribution(Distribution):
    def __init__(self):
        self.kurtosis = 3.0
        self.skewness = 0.0

    def inverse_cdf(self, u):
        from scipy.special import erfinv
        return np.multiply(np.sqrt(2), erfinv(np.subtract(np.multiply(2, u), 1)))

    def get_name(self, case_name):
        return 'Standard Normal Distribution'


class GenericDistribution(Distribution):
    def __init__(self, fcn):
        self.fcn = fcn

    def sample(self, sample_size):
        return self.fcn(sample_size)


def student(df):
    def rvs(size):
        return t.rvs(df, size=size)
    return rvs


def standard_logistic():
    def rvs(size):
        return logistic.rvs(size=size)
    return rvs


def my_cauchy(loc, scale):
    def rvs(size):
        return cauchy.rvs(loc=loc, scale=scale, size=size)
    return rvs


def standard_double_exp():
    def rvs(size):
        return laplace.rvs(size=size)
    return rvs


def gumbel(alpha, beta):
    def rvs(size):
        return gumbel_r.rvs(loc=alpha, scale=beta, size=size)
    return rvs


def weibull(alpha):
    def rvs(size):
        return weibull_min.rvs(alpha, size=size)
    return rvs


def lognormal(std):
    def rvs(size):
        return lognorm.rvs(std, size=size)
    return rvs

def my_gamma(alpha):
    def rvs(size):
        return gamma.rvs(alpha, size=size)
    return rvs


def exponential():
    def rvs(size):
        return expon.rvs(size=size)
    return rvs


def my_uniform():
    def rvs(size):
        return uniform.rvs(size=size)
    return rvs


def my_beta(a, b):

    def rvs(size):
        return beta.rvs(a, b, size=size)
    return rvs



