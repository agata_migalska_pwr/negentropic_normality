lambdas_matrix = [[0.0, 0.197454, 0.134915, 0.134915],
                  [0.0, 1.0, 1.4, 0.25],
                  [0.0, 1.0, 0.00007, 0.1],
                  [3.586508, 0.04306, 0.025213, 0.094029],
                  [0.0, -1.0, -0.0075, -0.03],
                  [-0.116734, -0.351663, -0.13, -0.16],
                  [0.0, -1.0, -0.1, -0.18],
                  [0.0, -1.0, -0.001, -0.13],
                  [0.0, -1.0, -0.0001, -0.17]]

skewness_matrix = [0.0, 0.5, 1.5, 0.9, 1.5, 0.8, 2.0, 3.16, 3.88]

kurtosis_matrix = [3.0, 2.2, 5.8, 4.2, 7.5, 11.4, 21.2, 23.8, 40.7]

print('\\textbf{Case} & $\\mathbf{\\lambda_1}$ & $\\mathbf{\\lambda_2}$ & $\\mathbf{\\lambda_3}$ & '
      '$\\mathbf{\\lambda_4}$ & \\textbf{Skewness} & \\textbf{Kurtosis} \\\\\\midrule')
for idx, lambdas in enumerate(lambdas_matrix):
    print('{} & ${}$ & ${}$ & ${}$ & ${}$ & ${}$ & ${}$ \\\\'
          .format(idx + 1, lambdas[0], lambdas[1], lambdas[2], lambdas[3], skewness_matrix[idx], kurtosis_matrix[idx]))

