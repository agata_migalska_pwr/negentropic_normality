from collections import namedtuple

PValue = namedtuple('PValue', ['k0', 'p'])
REJECT = True
ACCEPT = False